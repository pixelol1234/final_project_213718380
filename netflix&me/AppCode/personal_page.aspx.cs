﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class personal_page : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            profile.ImageUrl = "data:image;base64," + Convert.ToBase64String(Users.getImageById(currentUser.getID()));
            mail.Text = Users.getMailById(currentUser.getID());
            phone.Text = Users.getPhoneById(currentUser.getID());
            sumFr.Text = friends.getFriends(currentUser.getID(), currentUser.getUsername()).Count.ToString();
            sumReq.Text = friends.getReq(currentUser.getID()).Count.ToString();

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue != "personal_page.aspx")
                Server.Transfer(DropDownList1.SelectedValue);
        }

        protected void remove_Click(object sender, EventArgs e)
        {
            CurrentUser currentUser = (CurrentUser)Session["user"];
            byte[] image = { 0};
            Users.updatephoto(currentUser.getID(), image);
            Server.Transfer("personal_page.aspx");
        }

        protected void upload_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                CurrentUser currentUser = (CurrentUser)Session["user"];
                Stream im = FileUpload1.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(im);

                byte[] image  = br.ReadBytes((Int32)im.Length);
                byte[] imag  = FileUpload1.FileBytes;
                Users.updatephoto(currentUser.getID(), imag);
                Server.Transfer("personal_page.aspx");
            }
        }
    }
}