﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class ViewRequests : System.Web.UI.Page
    {
        /*
        * The method approves or rejects the selected user friend request.
        * The method decides whether to approve or reject or series by the command name button attribute.
        * The method takes the friend username from the command argument button attribute.
        */
        protected void btn_Click(object sender, EventArgs e)
        {
            CurrentUser currentUser = (CurrentUser)Session["user"];
            Button b = (Button)sender;
            CurrentUser friend = Users.FindByName(b.CommandArgument);
            if(b.CommandName == "Approve")
            {
                friends.confirmFriend(friend.getID(), currentUser.getID());
                Mail.sendMsg(Users.getMailById(friend.getID()), currentUser.getUsername() + " has accepted your friends request", "New friend");
            }
            else
            {
                friends.rejectFriend(friend.getID(), currentUser.getID());
                Mail.sendMsg(Users.getMailById(friend.getID()), currentUser.getUsername() + " has rejected your friends request", "New friend");
            }
            Server.Transfer("ViewRequests.aspx");

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            List<string> names = friends.getReq(currentUser.getID());
            Names.Text = "";
            foreach(string n in names)
            {
                Names.Text += n;
                Names.Text += "<br/>";
                Button b = new Button();
                b.Text = "Approve";
                b.ID = "Approve" + n;
                b.Click += btn_Click;
                b.CommandArgument = n;
                b.CommandName = "Approve";
                b.CssClass = "button";
                PlaceHolder1.Controls.Add(b);
                
                Button bt = new Button();
                bt.CssClass = "button";
                bt.Text = "Reject";
                bt.ID = "Reject" + n;
                bt.Click += btn_Click;
                bt.CommandArgument = n;
                bt.CommandName = "reject";
                PlaceHolder1.Controls.Add(bt);
                Label l = new Label();
                l.Text = "<br/>";
                PlaceHolder1.Controls.Add(l);
            }
        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue != "ViewRequests.aspx")
                Server.Transfer(DropDownList1.SelectedValue);
        }
    }
}