﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
namespace Netflix
{
    public partial class AdminForum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("AdminLogin.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            List<message> msg = Forum.GetMessages();
            Label l1 = new Label();
            l1.Text = "<br/>";
            if (!Page.IsPostBack)
            {
                foreach (message m in msg)
                {

                    ListItem l = new ListItem(m.getAuthor() + ": " + m.getMsg(), m.getAuthor() + "-:- " + m.getMsg());
                    CheckBoxList1.Items.Add(l);


                }
            }
            
        }

        protected void post_Click(object sender, EventArgs e)
        {
            Forum.postMessage("Admin", mess.Text);
            Server.Transfer("AdminForum.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            foreach (ListItem l in CheckBoxList1.Items)
            {
                if (l.Selected)
                {
                    string mail = "";
                    CurrentUser user = (CurrentUser)Session["user"];
                    string Author = l.Value.Substring(0, l.Value.IndexOf("-:-"));
                    string msg = l.Value.Substring(l.Value.IndexOf("-:-")+4);
                    Forum.DeleteMessage(Author, msg);
                    if (Author != "Admin")
                    {
                        CurrentUser current = Users.FindByName(Author);
                        mail = Users.getMailById(current.getID());
                        Mail.sendMsg(mail, "Your message was removed from the forum", "Notice");
                    }
                }

            }

            Server.Transfer("AdminForum.aspx");
        }
    }
}