﻿using Netflix.links;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using YoutubeExtractor;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Threading;
using System.Net;
using VideoLibrary;
using Simplify.Web.MessageBox;
using Simplify.Web.MessageBox.Responses;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class Trailers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable(new MySqlConnection(DB.connection)))
            {
                Server.Transfer("Error.aspx");
            }
            ShareWhatsapp.Visible = false;
            ShareFacebook.Visible = false;
            whatsapp.Visible = false;
            info.Visible = false;
            Download.Visible = false;
            Linking.Text = "";
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            if (!Page.IsPostBack)
            {
                links.Links link = new links.Links();
                List<string> name = new List<string>(link.names());
                ListItem l = new ListItem("Choose Movie/Series", "Choose Movie/Series");
                trailer.Items.Add(l);
                foreach (string n in name)
                {
                    l = new ListItem(n, n);
                    trailer.Items.Add(l);
                }
            }
            


        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue != "Trailers.aspx")
                Server.Transfer(DropDownList1.SelectedValue);
        }

        protected void trailer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(trailer.SelectedValue == "Choose Movie/Series")
            {
                Server.Transfer("Trailers.aspx");
            }
            ShareWhatsapp.Visible = true;
            info.Visible = true;
            links.Links link = new links.Links();
            HtmlIframe shareFace = new HtmlIframe();
            shareFace.Src = "https://www.facebook.com/plugins/share_button.php?" + "href=" + link.getLink(trailer.SelectedValue) + "&layout=button&size=large&width=77&height=28&appId";
            ShareFacebook.Controls.Add(shareFace);
            ShareFacebook.Visible = true;
            whatsapp.Visible = true;
            ShareWhatsapp.NavigateUrl = "whatsapp://send?text=Look at that Trailer i saw on NetflixMe - " + link.getLink(trailer.SelectedValue); //When clicking on link - share on whatsapp
            HtmlIframe i = new HtmlIframe();
            i.Attributes["height"] = "768px";
            i.Attributes["width"] = "1200px";
            i.Attributes["allowfullscreen"] = "allowfullscreen";
            PlaceHolder1.Controls.Clear();
            string src = "https://www.youtube.com/embed/" + link.getLink(trailer.SelectedValue);
            i.Src = src;
            PlaceHolder1.Controls.Add(i);
            Download.Visible = true;
            Linking.NavigateUrl = link.getBrowse(trailer.SelectedValue);
            Linking.Text = "Full Watch";
            Linking.Target = "_blank";
            
        }

        protected void Download_Click(object sender, EventArgs e)
        {
            links.Links link = new links.Links();
            string url = link.getLink(trailer.SelectedValue);
            Thread thdSyncRead = new Thread(new ThreadStart(dialo)); // Creates new thread
            thdSyncRead.SetApartmentState(ApartmentState.STA);
            thdSyncRead.Start();
            thdSyncRead.Join(); //So the function will resume only after the thread finished 
            string path = Session["path"].ToString();
            if(path != "")
            {
                var youTube = YouTube.Default; // starting point for YouTube actions
                var video = youTube.GetVideo(url); // gets a Video object with info about the video

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Video Downloaded!');", true);
                File.WriteAllBytes(path + @"\" + video.FullName, video.GetBytes());//Download the video
            }

                trailer.SelectedValue = "Choose Movie/Series";


        }
        /*
         * The method opens a dialog to choose a directory to save the video in.
         * The selected path is saved in a session
         */
        private void dialo()
        {
            CommonOpenFileDialog fileDialog = new CommonOpenFileDialog();
            string path = "";
            fileDialog.IsFolderPicker = true;
            fileDialog.InitialDirectory = "C:\\Users";
            CommonFileDialogResult result = fileDialog.ShowDialog();

            if (result == CommonFileDialogResult.Ok)
            {
                path = fileDialog.FileName;
            }
            Session["path"] = path;
        }
    }
}