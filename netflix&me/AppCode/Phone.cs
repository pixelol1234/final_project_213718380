﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;

namespace Netflix
{
    public class Phone
    {
        public static void sendPass(string userID)
        {
            string phoneNum = Users.getPhoneById(userID);

            var accountSid = "AC78198f21b3d73ce35d3c00aa209f0dad";
            var authToken = "1b9d587cdd9343da5b1d6f6fb0b1a49f";
            TwilioClient.Init(accountSid, authToken);
            string country = "+972";
            string phone ="whatsapp:"+ country+ phoneNum.Substring(1);
            var messageOptions = new CreateMessageOptions(
                new PhoneNumber(phone));
            messageOptions.From = new PhoneNumber("whatsapp:+14155238886");
            string password = Membership.GeneratePassword(7, 3);
            messageOptions.Body = "Your password is: *"+password+"*";
            Users.ChangePass(userID, password);
            var message = MessageResource.Create(messageOptions);
        }
    }
}