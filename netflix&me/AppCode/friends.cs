﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public class friends
        //confirm = 3
        //reject = 4
        //add = 1
    {
        public static bool confirmFriend(string from,string to)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"UPDATE friendship SET status = '3' WHERE (userSend='{from}' AND userGet='{to}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;
        }
        public static bool addFriend(string from, string to)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"INSERT INTO friendship (userSend, userGet, status) VALUES ('{from}', '{to}', '1');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;
        }
        public static bool rejectFriend(string from, string to)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"UPDATE friendship SET status = '4' WHERE (userSend='{from}' AND userGet='{to}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;
        }
        public static bool deleteFriend(string from, string to)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"DELETE FROM friendship WHERE ((userSend='{from}' AND userGet='{to}') or (userSend='{to}' AND userGet='{from}'));";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;
        }
        //The method returns all of the friends requests the user has
        public static List<string> getReq(string ID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT users.userName FROM `netflix&me`.friendship
                                inner join users on users.ID = friendship.userSend
                                where friendship.userGet='{ ID}' and friendship.status=1;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<string> names = new List<string>();
            while (r.Read())
            {
                names.Add(r.GetValue(0).ToString());
            }
            con.Close();
            return names;
        }
        //The method returns the list of all the user friends
        public static List<string> getFriends(string ID, string username)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT users.userName FROM `netflix&me`.friendship
                                inner join users on (users.ID = friendship.userGet or users.ID = friendship.userSend)
                                where (friendship.userGet='{ID}' or friendship.userSend='{ID}') and (friendship.status=3) and users.userName!='{username}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<string> names = new List<string>();
            
            while (r.Read())
            {
                names.Add(r.GetValue(0).ToString());
            }
            
            
            con.Close();
            return names;
        }
        //The method returns the list of all the user pending friend requests
        public static List<string> getFriendsAsked(string ID, string username)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT users.userName FROM `netflix&me`.friendship
                                inner join users on (users.ID = friendship.userGet or users.ID = friendship.userSend)
                                where (friendship.userGet='{ID}' or friendship.userSend='{ID}') and (friendship.status=3 or friendship.status=1) and users.userName!='{username}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<string> names = new List<string>();

            while (r.Read())
            {
                names.Add(r.GetValue(0).ToString());
            }


            con.Close();
            return names;
        }
        public static List<string> getAllUsers(string ID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT userName FROM `netflix&me`.users
                                where users.ID != '{ID}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<string> names = new List<string>();
            while (r.Read())
            {
                names.Add(r.GetValue(0).ToString());
            }
            con.Close();
            return names;
        }
        //The method return all of the users who are not the user friends
        public static List<string> getNonFriends(string ID, string name)
        {
            List<string> allUsers = getAllUsers(ID);
            List<string> Friends = getFriendsAsked(ID, name);
            List<string> nonFriends = new List<string>();
            bool found = false;

            foreach(string u in allUsers)
            {
                found = false;
                foreach(string f in Friends)
                {
                    if (u == f)
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    nonFriends.Add(u);
                }
            }
            return nonFriends;

        }
    }
}