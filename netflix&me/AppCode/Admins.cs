﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public class Admins
    {

        public static CurrentUser Login(string username, string ID, string password)
        {
            string connectionString = DB.connection;

            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        password,ID
                                FROM            admins
                                WHERE        (username = '{username}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            if (r != null)
            {
                string pass = r[0].ToString();
                string id = r[1].ToString();
                if (pass == password && id == ID)
                {
                    return new CurrentUser(password, username, ID);
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
        /*
         * The method deletes the selected user from all of the tables in the data base
         */
        public static void deleteUser(string username, string ID, string password)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"delete from friendship where (userGet='{ID}' OR userSend='{ID}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            SqlQuer = $@"delete from saveseries where (userId = '{ID}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            res = cmd.ExecuteNonQuery();
            con.Close();
            SqlQuer = $@"delete from savemovies where (userId = '{ID}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            res = cmd.ExecuteNonQuery();
            con.Close();
            SqlQuer = $@"delete from users where (userName = '{username}' AND password = '{password}' AND ID = '{ID}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            res = cmd.ExecuteNonQuery();
            con.Close();
        }
        /*
         * The method deletes the selected series from all of the tables in the data base
         */
        public static void deleteSeries(string name)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT id FROM serieses
                                WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            object[] vals = new object[1];
            r.GetValues(vals);
            int id = ((int)vals[0]);

            con.Close();
            SqlQuer = $@"delete from saveseries where (seriesID = '{id}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            SqlQuer = $@"delete from serieses where (id = '{id}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            res = cmd.ExecuteNonQuery();
            con.Close();

        }
        /*
         * The method deletes the selected movie from all of the tables in the data base
         */
        public static void deleteMovie(string name)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT id FROM movies
                                WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            object[] vals = new object[1];
            r.GetValues(vals);
            int id = ((int)vals[0]);

            con.Close();
            SqlQuer = $@"delete from savemovies where (movieID = '{id}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            SqlQuer = $@"delete from movies where (id = '{id}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            res = cmd.ExecuteNonQuery();
            con.Close();

        }
    }
}