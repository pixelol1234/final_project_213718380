﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netflix
{
    public class CurrentUser
    {
        private string username;
        public string getUsername()
        {
            return this.username;
        }
        public void setUsername(string username)
        {
            this.username = username;
        }
        private string ID;
        public string getID()
        {
            return this.ID;
        }
        public void setID(string id)
        {
            this.ID = id;
        }
        private string Pass;
        public string getPass()
        {
            return this.Pass;
        }
        public void setPass(string pass)
        {
            this.Pass = pass;
        }
        public CurrentUser(string password, string userName, string ID)
        {
            this.Pass = password;
            this.username = userName;
            this.ID = ID;
        }
    }
}